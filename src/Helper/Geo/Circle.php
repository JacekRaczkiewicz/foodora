<?php

namespace App\Helper\Geo;

class Circle
{
    /** @var float $radius*/
    private $radius;

    /** @var Point $center */
    private $center;

    /**
     * Circle constructor.
     * @param float $radius
     * @param Point $center
     */
    public function __construct(float $radius, Point $center)
    {
        $this->radius = $radius;
        $this->center = $center;
    }

    /**
     * @return float
     */
    public function getRadius(): float
    {
        return $this->radius;
    }

    /**
     * @param float $radius
     */
    public function setRadius(float $radius)
    {
        $this->radius = $radius;
    }

    /**
     * @return Point
     */
    public function getCenter(): Point
    {
        return $this->center;
    }

    /**
     * @param Point $center
     */
    public function setCenter(Point $center)
    {
        $this->center = $center;
    }

    /**
     * @param Point $point
     * @return boolean
     */
    public function within(Point $point): float {
        $lon1 = $this->getCenter()->getLongitude();
        $lat1 = $this->getCenter()->getLatitude();
        $lon2 = $point->getLongitude();
        $lat2 = $point->getLatitude();

        $distance = $this->distance($lat1, $lon1, $lat2, $lon2);

        if($distance <= $this->getRadius()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param float $lat1
     * @param float $lon1
     * @param float $lat2
     * @param float $lon2
     * @return float
     */
    private function distance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        //distance in kilometers
        return $miles * 1.609344;
    }

}
