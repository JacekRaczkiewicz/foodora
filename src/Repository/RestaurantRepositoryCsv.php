<?php

namespace App\Repository;

use App\DTO\InputDTO\Restaurant\SearchRestaurantInput;
use App\Service\LoadRestaurantsInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ExpressionBuilder;


class RestaurantRepositoryCsv implements RestaurantRepositoryInterface {

    /**
     * All restaurants loaded from source land here
     * @var ArrayCollection $list
     */
    private $list;
    /**
     * RestaurantRepository constructor.
     * @param LoadRestaurantsInterface $loadRestaurants
     */
    public function __construct(LoadRestaurantsInterface $loadRestaurants)
    {
        $this->list = $loadRestaurants->loadData();
    }

    /**
     * @param SearchRestaurantInput $searchRestaurantInput
     * @return ArrayCollection
     */

    public function search(SearchRestaurantInput $searchRestaurantInput): ArrayCollection {

        $criteria = new Criteria();

        if ($searchRestaurantInput->cuisine) {
            $criteria->andWhere(Criteria::expr()->eq('cuisine', $searchRestaurantInput->cuisine));
        }
        if ($searchRestaurantInput->city) {
            $criteria->andWhere(Criteria::expr()->eq('city', $searchRestaurantInput->city));
        }
        if ($searchRestaurantInput->name) {
            $criteria->andWhere(Criteria::expr()->eq('restaurantName', $searchRestaurantInput->name));
        }
        if ($searchRestaurantInput->freeText) {
            $criteria->andWhere(Criteria::expr()->orX(
                Criteria::expr()->contains('restaurantName', $searchRestaurantInput->freeText),
                Criteria::expr()->contains('cuisine', $searchRestaurantInput->freeText),
                Criteria::expr()->contains('city', $searchRestaurantInput->freeText),
                Criteria::expr()->contains('clientKey', $searchRestaurantInput->freeText)
                )
            );
        }

        $result = $this->list->matching($criteria);


        //Distance filtering
        if ($searchRestaurantInput->searchArea) {
            $circle = $searchRestaurantInput->searchArea;
            $result = $result->filter(function($restaurant) use($circle) {
                return $circle->within($restaurant->getLocation());
            });
        }

        return $result;
    }
}
