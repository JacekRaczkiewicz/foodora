<?php

namespace App\Repository;

use App\DTO\InputDTO\Restaurant\SearchRestaurantInput;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This interface allows easily switch from CSV datasource to Database
 */

interface RestaurantRepositoryInterface
{
    public function search(SearchRestaurantInput $searchRestaurantInput):ArrayCollection;
}
