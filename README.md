# Installation:
1.  Fetch repository: ````git clone git@bitbucket.org:JacekRaczkiewicz/foodora.git````
	
2.  go to installed folder: ````cd foodora````
	
3.  create containers: ````sudo docker-compose up -d````
	
4.  install all packages using: ````composer install````
    
5.  open in your browser :
    http://localhost:8888/restaurant/search - will display all restaurants
 
# Usage:
I made assumptions that filters are connected using logical AND, so every returned entry match all provided filters.

There are following filters available:

*  city - return restaurants from given city
*  name - return restaurants exact matching provided name
*  cuisine - return restaurants serving given cuisine
*  freeText - return restaurants when freeText is included in name or city or cuisine or clientKey
*  distance - for filtering by distance we need also the reference point, it is required to submit 'lat' and 'lng' parameters when 'distance' is present. Returns all restaurants that are closer (from given point) than 'distance'. 'distance', 'lat', 'lng' must be float numbers. distance is assumed to be in kilometers. API accept 'lat' and 'lng' within Sweden.

All filters are case sensitive
All filters take one value only. Possible improvement to allow multiple values connected with OR - for example cuisine=Sallad|Sushi

example for restaurants in Stockholm:

http://localhost:8888/restaurant/search?distance=12&lat=59.32&lng=18

or

http://localhost:8888/restaurant/search?city=Stockholm

# Tests:
Some integration tests
    ````
    run: ./bin/phpunit
    ````
