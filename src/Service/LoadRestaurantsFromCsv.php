<?php

namespace App\Service;

use App\Entity\Restaurant;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class LoadRestaurantsFromCsv
 * @package App\Service
 *
 */

class LoadRestaurantsFromCsv implements LoadRestaurantsInterface
{
    /** @var string $path */
    private $path;

    /**
     * LoadRestaurantsFromCsv constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * Load data from CSV and return an array of restaurants
     * return array
     */

    public function loadData()
    {
        $result = json_decode(file_get_contents($this->path));
        $restaurantArr = new ArrayCollection();
        foreach($result As $restaurant) {
            $restaurantArr->add(new Restaurant($restaurant->clientKey, $restaurant->restaurantName, $restaurant->cuisine, $restaurant->city, $restaurant->latitude, $restaurant->longitude));
        }

        return $restaurantArr;
    }
}
