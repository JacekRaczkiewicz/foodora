<?php

namespace App\Controller\Restaurant;

use App\DTO\InputDTO\Restaurant\SearchRestaurantInput;
use App\DTO\OutputDTO\ErrorOutput;
use App\DTO\OutputDTO\SuccessOutput;
use App\DTO\Query\Restaurant\SearchRestaurantQuery;
use App\Repository\RestaurantRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Swagger\Annotations AS SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SearchController {
    /** @var MessageBusInterface $messageBus */
    private $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @Route("/restaurant/search")
     *
     * * @Operation(
     *     tags={"search"},
     *     summary="search matching restaurants",
     *     @SWG\Parameter(
     *          name="name",
     *          in="query",
     *          type="string",
     *          description="Name of the restaurant"
     *     ),
     *     @SWG\Parameter(
     *          name="cuisine",
     *          in="query",
     *          type="string",
     *          description="Cuisine name"
     *     ),
     *     @SWG\Parameter(
     *          name="city",
     *          in="query",
     *          type="string",
     *          description="City of the restaurant"
     *     ),
     *     @SWG\Parameter(
     *          name="distance",
     *          in="query",
     *          type="float",
     *          description="Distance from given point (lat/lng)"
     *     ),
     *     @SWG\Parameter(
     *          name="lat",
     *          in="query",
     *          type="float",
     *          description="Lattitude used for calculating distance from restaurant"
     *     ),
     *     @SWG\Parameter(
     *          name="lng",
     *          in="query",
     *          type="float",
     *          description="Longitude used for calculating distance from restaurant"
     *     ),
     *     @SWG\Parameter(
     *          name="freeText",
     *          in="query",
     *          type="string",
     *          description="Free text search applied to all fields"
     *     ),
     *     @SWG\Response(
     *          response="200",
     *          description="Returned when successful",
     *     )
     * )
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response $response
     */
    public function searchRestaurant(Request $request, ValidatorInterface $validator): Response {
        $input = new SearchRestaurantInput($request);
        $errors = $validator->validate($input);
        if (count($errors) > 0) {
            //var_dump($errors);
            return new JsonResponse(new ErrorOutput(400, "Parameters validation failed", $errors), 400);
        }

        $query = new SearchRestaurantQuery($input);
        $this->messageBus->dispatch($query);
        return new JsonResponse(new SuccessOutput(200, $query->getOutput()), 200);
    }
}
