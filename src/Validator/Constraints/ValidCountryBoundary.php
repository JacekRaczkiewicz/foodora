<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @property float $latMin
 * @property float $latMax
 * @property float $lngMin
 * @property float $lngMax
 * @property string $country
 * @property string $message
 * @Annotation
 */
class ValidCountryBoundary extends Constraint
{
    public $message = 'This {{ type }} "{{ value }}" is invalid. Should be in {{ country }} between {{ min }} and {{ max }}.';
    //('Sweden', (11.0273686052, 55.3617373725, 23.9033785336, 69.1062472602)),
    public $latMin = 55.3617373725;
    public $latMax = 69.1062472602;
    public $lngMin = 11.0273686052;
    public $lngMax = 23.9033785336;
    public $country = "Sweden";
}
