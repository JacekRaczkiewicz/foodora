<?php

namespace App\DTO\InputDTO\Restaurant;

use App\Helper\Geo\Circle;
use App\Helper\Geo\Point;
use Symfony\Component\HttpFoundation\Request;
use App\Validator\Constraints as AcmeAssert;
use Symfony\Component\Validator\Constraints as Assert;


class SearchRestaurantInput
{
    /**
     * @Assert\Type("string")
     *
     * @var string|null $name
     */
    public $name;

    /**
     * @Assert\Type("string")
     *
     * @var string|null $cuisine
     */
    public $cuisine;

    /**
     * @Assert\Type("string")
     *
     * @var string|null $city
     */
    public $city;

    /**
     * @AcmeAssert\ValidCountryBoundary
     *
     * @var Circle|null $circle
     */
    public $searchArea;

    /**
     * @Assert\Type("string")
     *
     * @var string|null $freeText
     */
    public $freeText;

    /**
     * @Assert\Regex(
     *     pattern="/^\d*\.?\d*$/",
     *     match=true,
     *     message="distance must be float number"
     * )
     */
    public $distance;

    /**
     * @Assert\Regex(
     *     pattern="/^\d*\.?\d*$/",
     *     match=true,
     *     message="lat must be float number"
     * )
     */
    public $lat;

    /**
     * @Assert\Regex(
     *     pattern="/^\d*\.?\d*$/",
     *     match=true,
     *     message="lng must be float number"
     * )
     */
    public $lng;

    /**
     * SearchRestaurantInput constructor.
     * @param Request $request
     */
    public function __construct($request)
    {
        $this->name = $request->query->get('name');
        $this->city = $request->query->get('city');
        $this->cuisine = $request->query->get('cuisine');
        $this->freeText = $request->query->get('freeText');
        $this->distance = $request->query->get('distance');
        $this->lat = $request->query->get('lat');
        $this->lng = $request->query->get('lng');

        $distance = (float) $this->distance;
        if($distance) {
            $this->searchArea = new Circle(
                $distance,
                new Point(
                    (float) $this->lat,
                    (float) $this->lng
                )
            );
        }


    }
}
