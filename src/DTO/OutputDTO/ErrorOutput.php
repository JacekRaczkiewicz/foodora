<?php
namespace App\DTO\OutputDTO;

class ErrorOutput extends BaseOutput
{
    /** @var string $error */
    public $error;

    /** @var string[]  */
    public $errorMessages;

    /**
     * ErrorOutput constructor.
     * @param int $status
     * @param string $error
     * @param $errorMessages
     */
    public function __construct(int $status, string $error, $errorMessages = null)
    {
        $this->status = $status;
        $this->error = $error;
        if ($errorMessages) {

            $this->errorMessages = [];
            foreach($errorMessages AS $error) {
                $this->errorMessages[] = $error->getMessage();
            }
        }

    }
}
