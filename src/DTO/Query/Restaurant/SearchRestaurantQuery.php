<?php

namespace App\DTO\Query\Restaurant;

use App\DTO\InputDTO\Restaurant\SearchRestaurantInput;

class SearchRestaurantQuery
{
    /** @var SearchRestaurantInput $searchRestaurantInput */
    private $searchRestaurantInput;

    /** @var array $output */
    private $output;

    /**
     * SearchRestaurantQuery constructor.
     * @param SearchRestaurantInput $searchRestaurantInput
     */
    public function __construct(SearchRestaurantInput $searchRestaurantInput)
    {
        $this->searchRestaurantInput = $searchRestaurantInput;
    }

    /**
     * @return SearchRestaurantInput
     */
    public function getSearchRestaurantInput(): SearchRestaurantInput
    {
        return $this->searchRestaurantInput;
    }

    /**
     * @param SearchRestaurantInput $searchRestaurantInput
     */
    public function setSearchRestaurantInput(SearchRestaurantInput $searchRestaurantInput)
    {
        $this->searchRestaurantInput = $searchRestaurantInput;
    }

    /**
     * @return array
     */
    public function getOutput(): array
    {
        return $this->output;
    }

    /**
     * @param array $output
     */
    public function setOutput(array $output)
    {
        $this->output = $output;
    }



}
