<?php

namespace App\DTO\OutputDTO\Restaurant;


class RestaurantOutput
{
    /** @var string $clientKey*/
    public $clientKey;

    /** @var string $restaurantName*/
    public $restaurantName;

    /** @var string $cuisine*/
    public $cuisine;

    /** @var string $city*/
    public $city;

    /** @var float $latitude*/
    public $latitude;

    /** @var float $longitude*/
    public $longitude;
}

