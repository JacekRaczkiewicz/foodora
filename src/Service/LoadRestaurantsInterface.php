<?php

namespace App\Service;
/**
 * Interface LoadRestaurantsInterface
 * @package App\Service
 */

interface LoadRestaurantsInterface {
    public function loadData();
}
