<?php

namespace App\Handler\Restaurant;

use App\DTO\Query\Restaurant\SearchRestaurantQuery;
use App\Entity\Restaurant;
use App\Factory\Restaurant\RestaurantOutputFactory;
use App\Repository\RestaurantRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SearchRestaurantHandler implements MessageHandlerInterface
{

    /** @var RestaurantRepositoryInterface $restaurantRepository */
    private $restaurantRepository;

    /** @var RestaurantOutputFactory $restaurantOutputFactory */
    private $restaurantOutputFactory;

    /**
     * SearchRestaurantHandler constructor.
     * @param RestaurantRepositoryInterface $restaurantRepository
     * @param RestaurantOutputFactory $restaurantOutputFactory
     */
    public function __construct(RestaurantRepositoryInterface $restaurantRepository, RestaurantOutputFactory $restaurantOutputFactory)
    {
        $this->restaurantRepository = $restaurantRepository;
        $this->restaurantOutputFactory = $restaurantOutputFactory;
    }

    public function __invoke(SearchRestaurantQuery $searchRestaurantQuery)
    {
        // TODO: Implement __invoke() method.
        $restaurants = $this->restaurantRepository->search($searchRestaurantQuery->getSearchRestaurantInput());
        $restaurantOutput = $restaurants->map(function(Restaurant $restaurant) {
            return $this->restaurantOutputFactory->create($restaurant);
        });

        $searchRestaurantQuery->setOutput($restaurantOutput->getValues());
    }
}
