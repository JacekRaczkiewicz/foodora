<?php

namespace App\Entity;

use App\Helper\Geo\Point;

class Restaurant {

    /**
     * @var string $clientKey
     */
    private $clientKey;

    /**
     * @var string $restaurantName
     */
    private $restaurantName;

    /**
     * @var string $cuisine
     */
    private $cuisine;

    /**
     * @var string $city
     */
    private $city;

    /**
     * @var Point $location
     */
    private $location;

    /**
     * Restaurant constructor.
     * @param string $clientKey
     * @param string $restaurantName
     * @param string $cuisine
     * @param string $city
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct(string $clientKey, string $restaurantName, string $cuisine, string $city, float $latitude, float $longitude)
    {
        $this->clientKey = $clientKey;
        $this->restaurantName = $restaurantName;
        $this->cuisine = $cuisine;
        $this->city = $city;
        $point = new Point($latitude, $longitude);
        $this->location = $point;
    }

    /**
     * @return string
     */
    public function getClientKey(): string
    {
        return $this->clientKey;
    }

    /**
     * @param string $clientKey
     */
    public function setClientKey(string $clientKey)
    {
        $this->clientKey = $clientKey;
    }

    /**
     * @return string
     */
    public function getRestaurantName(): string
    {
        return $this->restaurantName;
    }

    /**
     * @param string $restaurantName
     */
    public function setRestaurantName(string $restaurantName)
    {
        $this->restaurantName = $restaurantName;
    }

    /**
     * @return string
     */
    public function getCuisine(): string
    {
        return $this->cuisine;
    }

    /**
     * @param string $cuisine
     */
    public function setCuisine(string $cuisine)
    {
        $this->cuisine = $cuisine;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * @return Point
     */
    public function getLocation(): Point
    {
        return $this->location;
    }

    /**
     * @param Point $location
     */
    public function setLocation(Point $location)
    {
        $this->location = $location;
    }






}
