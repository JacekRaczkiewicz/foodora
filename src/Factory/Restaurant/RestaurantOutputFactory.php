<?php

namespace App\Factory\Restaurant;


use App\DTO\OutputDTO\Restaurant\RestaurantOutput;
use App\Entity\Restaurant;

class RestaurantOutputFactory
{
    public function create(Restaurant $restaurant): RestaurantOutput
    {
        $output = new RestaurantOutput();
        $output->clientKey = $restaurant->getClientKey();
        $output->restaurantName = $restaurant->getRestaurantName();
        $output->cuisine = $restaurant->getCuisine();
        $output->city = $restaurant->getCity();
        $output->latitude = $restaurant->getLocation()->getLatitude();
        $output->longitude = $restaurant->getLocation()->getLongitude();

        return $output;
    }
}
