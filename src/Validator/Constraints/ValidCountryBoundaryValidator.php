<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class ValidCountryBoundaryValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ValidCountryBoundary) {
            throw new UnexpectedTypeException($constraint, ValidCountryBoundary::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (!($value->getCenter()->getLatitude() && $value->getCenter()->getLongitude())) {
            if (!$value->getCenter()->getLatitude()) {
                $this->context->buildViolation("Missing latitude")->addViolation();
            }
            if (!$value->getCenter()->getLongitude()) {
                $this->context->buildViolation("Missing longitude")->addViolation();
            }
        } else {

            if ($value->getCenter()->getLatitude() > $constraint->latMax ||
                $value->getCenter()->getLatitude() < $constraint->latMin
            ) {
                // Must be withing lat boundaries
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $value->getCenter()->getLatitude())
                    ->setParameter('{{ min }}', $constraint->latMin)
                    ->setParameter('{{ max }}', $constraint->latMax)
                    ->setParameter('{{ country }}', $constraint->country)
                    ->setParameter('{{ type }}', 'latitude')
                    ->addViolation();
            }

            if ($value->getCenter()->getLongitude() > $constraint->lngMax ||
                $value->getCenter()->getLongitude() < $constraint->lngMin
            ) {
                // Must be withing lat boundaries
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $value->getCenter()->getLongitude())
                    ->setParameter('{{ min }}', $constraint->lngMin)
                    ->setParameter('{{ max }}', $constraint->lngMax)
                    ->setParameter('{{ country }}', $constraint->country)
                    ->setParameter('{{ type }}', 'longitude')
                    ->addViolation();
            }
        }
    }
}

