<?php

namespace App\DTO\OutputDTO;

class SuccessOutput extends BaseOutput
{

    /**
     * ErrorOutput constructor.
     * @param int $status
     * @param array|null $data
     */
    public function __construct(int $status, $data)
    {
        $this->status = $status;
        $this->data = $data;
    }
}
