<?php

namespace App\Tests\Controller\Restaurant;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SearchControllerTest extends WebTestCase
{
    public function testSearchRestaurantReturnCode() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testSearchRestaurantMissingLat() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?distance=4&lng=12');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testSearchRestaurantMissingLng() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?distance=4&lat=12');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testSearchRestaurantMissingLatLng() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?distance=4');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testSearchRestaurantWrongLatLng() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?distance=4&lat=22&lng=22');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testSearchRestaurantGoodLatLng() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?distance=4&lat=59&lng=18');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testSearchRestaurantGoodLatLngNoResult() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?distance=0.4&lat=59&lng=18');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $result = json_decode($client->getResponse()->getContent());
        $this->assertEquals([],$result->data);
    }

    public function testSearchRestaurantCityFilter() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?city=Malmö');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $result = json_decode($client->getResponse()->getContent());
        $this->assertEquals(170,count($result->data));
    }

    public function testSearchRestaurantCuisineFilter() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?cuisine=Sallad');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $result = json_decode($client->getResponse()->getContent());
        $this->assertEquals(24,count($result->data));
    }

    public function testSearchRestaurantNameFilter() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?name=KFC');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $result = json_decode($client->getResponse()->getContent());
        $this->assertEquals(2,count($result->data));
    }

    public function testSearchRestaurantFreeTextFilter() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?freeText=KFC');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $result = json_decode($client->getResponse()->getContent());
        $this->assertEquals(4,count($result->data));
    }

    public function testSearchRestaurantFreeTextCityFilter() {
        $client = static::createClient();
        $client->request('GET', '/restaurant/search?freeText=KFC&city=Lund');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $result = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1,count($result->data));
    }

}
